package liveplus;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseClass 
{
	public static WebDriver driver;
	
	@BeforeClass
	public void configBC()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\\\\\\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://bsedemo.com/dev/expertplus/");
	
	}
	
	/*@BeforeMethod
	public void configBM()
	{
		
		
	}
	
	@AfterMethod
	public void configAM()
	{
		
		
	}*/

	@AfterClass
	public void configAC()
	{
		//driver.close();
	}
	
	
	
}
