package liveplus;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Register extends BaseClass
{
	public static String email = "bsetec123@inboxbear.com";
	@Test(enabled=false)
	//here restricted to execute this block
	public void register()
	{
		BaseClass.driver.navigate().to("https://bsedemo.com/dev/expertplus/user/register");
		driver.findElement(By.name("firstname")).sendKeys("bsetec");
		driver.findElement(By.name("lastname")).sendKeys("test");
		driver.findElement(By.name("username")).sendKeys("bse-tec-test");
		driver.findElement(By.name("email")).sendKeys(email);
		driver.findElement(By.id("password")).sendKeys("bsetec123");
		driver.findElement(By.name("password_confirmation")).sendKeys("bsetec123");
		driver.findElement(By.xpath("//button[@type='submit']")).submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='alert alert-success fade in block-inner']"))));
		
		driver.navigate().to("https://app.inboxbear.com/inbox/");
		driver.findElement(By.xpath("//span[@class='bg-green fg-white badge hidden']/span")).click();
		driver.findElement(By.linkText("Active my account now")).click();
		
	}
// same like above restricted execution
	@Test(enabled=false)
	public void login() throws InterruptedException
	{
		
		driver.findElement(By.xpath("//a[text()='Sign In']")).click();
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.name("password")).sendKeys("bsetec123");
		
		Thread.sleep(500);
		driver.findElement(By.xpath("//button[text()=' Submit ']")).submit();
		driver.get("https://bsedemo.com/dev/expertplus/user/profile");
		driver.findElement(By.name("designation")).sendKeys("QA");
		
		driver.findElement(By.name("headline")).sendKeys("Bsetec Test");
		
		driver.findElement(By.xpath("//div[@class='note-editable panel-body']")).sendKeys("Hello, how are you?");
				
		WebElement language = driver.findElement(By.xpath("//select[@name='lang']"));
		
		Select sel = new Select(language);
		sel.selectByVisibleText("English (US)");
		
		driver.findElement(By.xpath("//html//div[@class='link_block']/div[1]/input[1]")).sendKeys("www.bsetec.com");
		/*driver.findElement(By.xpath("//html//div[@class='link_block']/div[2]/input[1]")).sendKeys("www.google.com");
		driver.findElement(By.xpath("//html//div[@class='link_block']/div[3]/input[1]")).sendKeys("www.twitter.com");
		driver.findElement(By.xpath("//html//div[@class='link_block']/div[4]/input[1]")).sendKeys("www.facebook.com");
		driver.findElement(By.xpath("//html//div[@class='link_block']/div[5]/input[1]")).sendKeys("www.linkedin.com");
		driver.findElement(By.xpath("//html//div[@class='link_block']/div[6]/input[1]")).sendKeys("www.youtube.com");*/
		Thread.sleep(500);
		driver.findElement(By.xpath("//button[@class='btn btn-color']")).click();
		
		
		String designation= driver.findElement(By.name("designation")).getAttribute("value");
		String expDesignation = "QA";
		Assert.assertEquals(designation, expDesignation);
		
		String headline= driver.findElement(By.name("headline")).getAttribute("value");
		String expHeadline = "Bsetec Test";
		Assert.assertEquals(headline, expHeadline);
		
		String firstName= driver.findElement(By.name("first_name")).getAttribute("value");
		String expfirstName = "bsetec";
		Assert.assertEquals(firstName, expfirstName);
		
		String lastName= driver.findElement(By.name("last_name")).getAttribute("value");
		String expLastName = "test";
		Assert.assertEquals(lastName, expLastName);
		
	}
	// as of now we execution happening from  this block
	@Test
	 public void profileImage() throws Throwable 
	 {
		//this is for login
		driver.findElement(By.xpath("//a[text()='Sign In']")).click();
		driver.findElement(By.id("email")).sendKeys(email);
		driver.findElement(By.name("password")).sendKeys("bsetec123");
		
		Thread.sleep(500);
		driver.findElement(By.xpath("//button[text()=' Submit ']")).submit();
		
		//page redirecting to profile page
		driver.navigate().to("https://bsedemo.com/dev/expertplus/user/photo"); 
		
		driver.findElement(By.name("cImage")).click();
		
		StringSelection stringSelection = new StringSelection("D:\\Image's\\HD\\4.jpg");
		Thread.sleep(2000);
		   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
		   
		
		
		   Robot robot;
		  // while(true)
		//   {
	//	try {
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
	           robot.keyPress(KeyEvent.VK_V);
	           robot.keyRelease(KeyEvent.VK_V);
	           robot.keyRelease(KeyEvent.VK_CONTROL);
	           robot.keyPress(KeyEvent.VK_ENTER);
	           robot.keyRelease(KeyEvent.VK_ENTER);
		//} catch (AWTException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		//}
		 //  }
		
		 
	 }


	
	
}


 



