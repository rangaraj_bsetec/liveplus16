package liveplus;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Login_logout {
public WebDriver driver;
	
	@Test(priority =1)
	public void launchbrowser() throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "D:\\\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.get("https://bsedemo.com:4600");
	driver.manage().window().maximize();
	Thread.sleep(2000);
	}
	@Test(priority =2)
	public void singinAsUser() throws InterruptedException, AWTException {
	//Login
	driver.findElement(By.cssSelector("body > app-root > app-base > div.liveplus-home.guestUser > mat-sidenav-container > mat-sidenav-content > div.top-wrap > div > div > div.col-md-10.col-sm-12.col-xs-12 > div > nav > ul > li:nth-child(1) > a")).click();
	//save current window
	String parentwindow = driver.getWindowHandle();
	Thread.sleep(2000);
	Robot robot = new Robot();
	//robot.keyPress(KeyEvent.VK_TAB);
	robot.keyPress(KeyEvent.VK_DOWN);
	driver.findElement(By.cssSelector("#mat-input-0")).sendKeys("rossignol@gmail.com");
	driver.findElement(By.cssSelector("#mat-input-1")).sendKeys("123456");
	driver.findElement(By.className("mat-raised-button")).click();
	//System.out.println("Successfully Login to application");
	Thread.sleep(3000);
	}
	@Test(priority = 3)
	public void LogoutFromuserLogin() {
	//Logout
	driver.findElement(By.cssSelector("body > app-root > app-base > div.liveplus-home.loggedUser > mat-sidenav-container > mat-sidenav-content > div.top-wrap > div > div > div.col-md-10.col-sm-12.col-xs-12 > div > nav > ul > li:nth-child(4) > a")).click();
	
	//System.out.println("Successfully logout form the application");
	/*
	for( String winhandle : driver.getWindowHandles()) {
		driver.switchTo().window(winhandle);
	}
	*/
		//close login popup
	//driver.findElement(By.cssSelector("#mat-dialog-3 > app-login > div > div > div > button > mat-icon")).click();
	/*
	Thread.sleep(1000);
	String login_window = driver.getWindowHandle();
	driver.switchTo().window(login_window);
	System.out.println("switch to popup");
	*/
	//driver.findElement(By.cssSelector("#mat-dialog-3 > app-login > div > div > div")).click();

	//Thread.sleep(2000);
	/*
	WebElement email = driver.findElement(By.xpath("//*[@id=\"mat-input-4\"]"));
	System.out.println("Find path");
	email.click();
	//sendKeys("rossignol@gmail.com");
	 */
	// driver.findElement(By.xpath("//*[@id=\"mat-input-6\"]"));
	////*[@id="mat-input-4"]
	driver.close();
	
}
}
