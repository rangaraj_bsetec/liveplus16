package liveplus;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Admin_login_logout {
public WebDriver driver;
	
	@Test(priority =1)
	public void launchbrowser() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://bsedemo.com:4600");
		driver.manage().window().maximize();
		Thread.sleep(2000);
	}
	@Test(priority =2)
	public void LoginAsAdmin() throws InterruptedException, AWTException {
		//Login
		driver.findElement(By.cssSelector("body > app-root > app-base > div.liveplus-home.guestUser > mat-sidenav-container > mat-sidenav-content > div.top-wrap > div > div > div.col-md-10.col-sm-12.col-xs-12 > div > nav > ul > li:nth-child(1) > a")).click();
		//save current window
		String parentwindow = driver.getWindowHandle();
		Thread.sleep(2000);
		Robot robot = new Robot();
		//robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_DOWN);
		driver.findElement(By.cssSelector("#mat-input-0")).sendKeys("admin@mail.com");
		driver.findElement(By.cssSelector("#mat-input-1")).sendKeys("123456");
		driver.findElement(By.className("mat-raised-button")).click();
		//System.out.println("Successfully Login to application");
		Thread.sleep(3000);
	}
	@Test(priority = 3)
	public void LogoutFromAdmin() throws InterruptedException {
		//Logout
		driver.findElement(By.cssSelector("body > app-root > app-base > div.liveplus-home.loggedUser > mat-sidenav-container > mat-sidenav-content > div.top-wrap > div > div > div.col-md-10.col-sm-12.col-xs-12 > div > nav > ul > li:nth-child(4) > a")).click();
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("body > app-root > app-admin > mat-sidenav-container > mat-sidenav-content > div > div > div > div.col-sm-4.top-rightblock > div > ul > li:nth-child(1) > a > span")).click();
		//System.out.println("Successfully logout form the application");
		driver.close();
}
	}
